package ua.knu.csc.is.pntools.unfolding.declarations;

import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.cyclicenumerations.CyclicEnumeration;

public class CyclicEnumerationDescriptor
		extends EnumerationDescriptor<CyclicEnumeration> {
	
	public CyclicEnumerationDescriptor(CyclicEnumeration sort) {
		super(sort);
	}

}
