package ua.knu.csc.is.pntools.unfolding.declarations;

public class FEConstantDescriptor {
	private final EnumerationDescriptor sortDescriptor;
	private final int index;
	
	public FEConstantDescriptor(EnumerationDescriptor sortDescriptor, int index) {
		this.sortDescriptor = sortDescriptor;
		this.index = index;
	}
	
	public EnumerationDescriptor getSortDescriptor() {
		return sortDescriptor;
	}
	
	public int getIndex() {
		return index;
	}
	
	public boolean isCyclic() {
		return sortDescriptor instanceof CyclicEnumerationDescriptor;
	}
	
	public FEConstantDescriptor succ() {
		return new FEConstantDescriptor(sortDescriptor, 
				sortDescriptor.getCardinality() - index > 1 ? index + 1 : 0);
	}
	
	public FEConstantDescriptor pred() {
		return new FEConstantDescriptor(sortDescriptor, 
				index == 0 ? sortDescriptor.getCardinality() - 1 : index - 1);
	}
	
}
