package ua.knu.csc.is.pntools.unfolding.declarations;

public class VariableDescriptor {
	
	private SortDescriptor sortDescriptor;
	private int index;
	
	public SortDescriptor getSortDescriptor() {
		return sortDescriptor;
	}
	
	public void setSortDescriptor(SortDescriptor sortDescriptor) {
		this.sortDescriptor = sortDescriptor;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

}
