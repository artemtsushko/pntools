package ua.knu.csc.is.pntools.unfolding.declarations;

import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.Sort;

public interface SortDescriptor<SORT extends Sort, CONST> {
	
	SORT getSort();
	
	int getCardinality();
	
	int getIndex(CONST constant);
	
	CONST getConstant(int index);
	
	String toString(CONST constant);
	
	default String toString(int index) {
		return toString(getConstant(index));
	}
	
}
