package ua.knu.csc.is.pntools.unfolding.declarations;

import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.dots.Dot;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.dots.DotConstant;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.dots.DotsFactory;

public class DotSortDescriptor implements SortDescriptor<Dot, DotConstant> {
	private Dot sort;
	private DotConstant constant;
	
	public DotSortDescriptor(Dot sort) {
		this.sort = sort;
		this.constant = DotsFactory.eINSTANCE.createDotConstant();
	}

	@Override
	public Dot getSort() {
		return sort;
	}

	@Override
	public int getCardinality() {
		return 1;
	}

	@Override
	public int getIndex(DotConstant constant) {
		return 0;
	}

	@Override
	public DotConstant getConstant(int index) {
		return constant;
	}

	@Override
	public String toString(DotConstant constant) {
		return "dot";
	}
	

}
