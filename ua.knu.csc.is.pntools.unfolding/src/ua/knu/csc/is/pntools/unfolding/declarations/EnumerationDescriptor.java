package ua.knu.csc.is.pntools.unfolding.declarations;

import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteenumerations.FEConstant;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteenumerations.FiniteEnumeration;

public class EnumerationDescriptor<SORT extends FiniteEnumeration> implements SortDescriptor<SORT, FEConstant> {

	protected SORT sort;
	protected Map<FEConstant, Integer> constantIndices = new HashMap<>();
	
	public EnumerationDescriptor(SORT sort) {
		this.sort = sort;
		for (ListIterator<FEConstant> it = sort.getElements().listIterator(); it.hasNext(); ) {
			constantIndices.put(it.next(), it.previousIndex());
		}
	}
	
	@Override
	public SORT getSort() {
		return sort;
	}

	@Override
	public int getCardinality() {
		return sort.getElements().size();
	}

	@Override
	public int getIndex(FEConstant constant) {
		Integer index = constantIndices.get(constant);
		if (index == null) {
			throw new IllegalArgumentException("Constant " + constant + 
					" is not enumerated in this enumeration: " + sort);
		}
		return index;
	}

	@Override
	public FEConstant getConstant(int index) {
		if (index >= getCardinality()) {
			throw new IndexOutOfBoundsException("Enumeration cardinality: " +
					getCardinality() + ", index: " + index);
		}
		return sort.getElements().get(index);
	}

	@Override
	public String toString(FEConstant constant) {
		return constant.getName() != null 
				? constant.getName()
				: constant.getId();
	}
	
	public FEConstantDescriptor findFEConstant(FEConstant constant) {
		Integer index = constantIndices.get(constant);
		if (index != null) {
			return new FEConstantDescriptor(this, index);
		} else {
			return null;
		}
	}

}
