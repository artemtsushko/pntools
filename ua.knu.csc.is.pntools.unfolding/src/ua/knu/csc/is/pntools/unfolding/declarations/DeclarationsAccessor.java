package ua.knu.csc.is.pntools.unfolding.declarations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.pnml.tools.epnk.helpers.NetFunctions;
import org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.HLPN;
import org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.Page;
import org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.Place;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.Bool;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.cyclicenumerations.CyclicEnumeration;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.dots.Dot;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteenumerations.FEConstant;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteenumerations.FiniteEnumeration;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.FiniteIntRange;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.Declaration;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.NamedSort;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.Sort;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.SortDecl;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.UserSort;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.Variable;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.VariableDecl;

import ua.knu.csc.is.pntools.unfolding.UnfoldingException;

public class DeclarationsAccessor {
	private HLPN petriNet;
	private Map<SortDecl, SortDescriptor> sortDescriptors = new HashMap<>();
	private Map<VariableDecl, VariableDescriptor> varDescriptors = new HashMap<>();
	private List<VariableDescriptor> varDescriptorsList = new ArrayList<>();
	
	public DeclarationsAccessor(HLPN petriNet) {
		this.petriNet = petriNet;
		processDeclarations();
	}

	
	private void processDeclarations() {
		processDeclarationsOnNet();
		processDeclarationsOnPages();
		resolveVariableSortDescriptors();
	}
	
	private void processDeclarationsOnNet() {
		if (petriNet.getDeclaration() != null) {
			for (org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.Declaration decl : petriNet.getDeclaration()) {
				if (decl.getStructure() != null && decl.getStructure().getDeclaration() != null) {
					for (Declaration declaration : decl.getStructure().getDeclaration()) {
						processDeclaration(declaration);
					}
				}
			}
		}
	}
	
	private void processDeclarationsOnPages() {
		for (org.pnml.tools.epnk.pnmlcoremodel.Page rootPage : petriNet.getPage()) {
			for (org.pnml.tools.epnk.pnmlcoremodel.Page subPage : 
				NetFunctions.getAllSubPages(rootPage)) {
				Page page = (Page) subPage;
				if (page.getDeclaration() != null) {
					for (org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.Declaration decl : page.getDeclaration()) {
						if (decl.getStructure() != null && decl.getStructure().getDeclaration() != null) {
							for (Declaration declaration : decl.getStructure().getDeclaration()) {
								processDeclaration(declaration);
							}
						}
					}
				}
			}
		}
	}
	
	private void processDeclaration(Declaration declaration) {
		if (declaration instanceof NamedSort) {
			processSortDeclaration((NamedSort) declaration);
		} else if (declaration instanceof VariableDecl) {
			processVariableDeclaration((VariableDecl) declaration);
		} else {
			throw new UnfoldingException(
					"Declaration not supported: " + declaration);
		}
	}
	
	private void processSortDeclaration(NamedSort sortDeclaration) {
		Sort sort = sortDeclaration.getDef();
		SortDescriptor descriptor;
		if (sort instanceof Dot) {
			descriptor = new DotSortDescriptor((Dot) sort);
		} else if (sort instanceof Bool) {
			descriptor = new BooleanSortDescriptor((Bool) sort);
		} else if (sort instanceof FiniteIntRange) {
			descriptor = new FiniteIntRangeDescriptor((FiniteIntRange) sort);
		} else if (sort instanceof CyclicEnumeration) {
			descriptor = new CyclicEnumerationDescriptor((CyclicEnumeration) sort);
		} else if (sort instanceof FiniteEnumeration) {
			descriptor = new FiniteEnumerationDescriptor((FiniteEnumeration) sort);
		} else {
			throw new UnfoldingException("Sort " + sort + " is not supported");
		}
		sortDescriptors.put(sortDeclaration, descriptor);
	}
	
	private void processVariableDeclaration(VariableDecl variableDecl) {
		VariableDescriptor variableDescriptor = new VariableDescriptor();
		variableDescriptor.setIndex(varDescriptorsList.size());
		varDescriptorsList.add(variableDescriptor);
		varDescriptors.put(variableDecl, variableDescriptor);
	}
	
	private void resolveVariableSortDescriptors() {
		varDescriptors.forEach((variableDeclaration, variableDescriptor) -> {
			if (variableDeclaration.getSort() instanceof UserSort) {
				UserSort sort = (UserSort) variableDeclaration.getSort();
				SortDescriptor sortDescriptor = sortDescriptors.get(sort.getDeclaration());
				assert sortDescriptor != null;
				variableDescriptor.setSortDescriptor(sortDescriptor);
			} else {
				throw new UnfoldingException("Only variables over user sorts are supported");
			}
		});
	}
	
	public int getNumSorts() {
		return sortDescriptors.size();
	}
	
	public int getNumVars() {
		return varDescriptors.size();
	}
	
	public VariableDescriptor resolveVariable(Variable variable) {
		return varDescriptors.get(variable.getRefvariable());
	}
	
	public VariableDescriptor getVariableDescriptor(int index) {
		return varDescriptorsList.get(index);
	}
	
	public SortDescriptor resolvePlaceSort(Place place) {
		Sort sort = place.getType().getStructure();
		if (sort instanceof UserSort) {
			SortDescriptor sortDescriptor = sortDescriptors
					.get(((UserSort) sort).getDeclaration());
			assert sortDescriptor != null;
			return sortDescriptor;
		} else {
			throw new UnfoldingException("Place sorts are supposed to be user sorts");
		}
	}
	
	public FEConstantDescriptor findFEConstant(FEConstant constant, 
											   List<Integer> variableIndices) {
		return variableIndices.stream()
				.map(varDescriptorsList::get)
				.map(VariableDescriptor::getSortDescriptor)
				.filter(d -> d instanceof EnumerationDescriptor)
				.map(d -> (EnumerationDescriptor) d)
				.map(sortDescriptor -> sortDescriptor.findFEConstant(constant))
				.filter(Objects::nonNull)
				.findAny().get();
	}

}
