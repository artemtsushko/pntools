package ua.knu.csc.is.pntools.unfolding.declarations;

import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteenumerations.FiniteEnumeration;

public class FiniteEnumerationDescriptor
		extends EnumerationDescriptor<FiniteEnumeration> {
	
	public FiniteEnumerationDescriptor(FiniteEnumeration sort) {
		super(sort);
	}

}
