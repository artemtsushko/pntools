package ua.knu.csc.is.pntools.unfolding.algorithm;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.And;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.BooleanOperator;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.Equality;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.Imply;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.Inequality;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.Not;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.Or;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.cyclicenumerations.CyclicEnumOperator;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.cyclicenumerations.Predecessor;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.cyclicenumerations.Successor;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteenumerations.FEConstant;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.FiniteIntRangeOperator;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.GreaterThan;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.GreaterThanOrEqual;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.LessThan;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.LessThanOrEqual;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.integers.NumberConstant;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.multisets.Add;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.multisets.All;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.multisets.Empty;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.multisets.NumberOf;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.multisets.Subtract;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.BuiltInConst;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.BuiltInOperator;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.OperatorDecl;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.Term;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.UserOperator;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.Variable;

import ua.knu.csc.is.pntools.unfolding.UnfoldingException;
import ua.knu.csc.is.pntools.unfolding.declarations.DeclarationsAccessor;
import ua.knu.csc.is.pntools.unfolding.declarations.SortDescriptor;
import ua.knu.csc.is.pntools.unfolding.declarations.VariableDescriptor;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.ImmutableMultiset;
import com.google.common.collect.Multiset;

public class MultisetTermEvaluator {
	private final SortDescriptor sortDescriptor;
	private final Map<VariableDescriptor, Integer> binding;
	private final DeclarationsAccessor declarations;
	
	public MultisetTermEvaluator(
			SortDescriptor sortDescriptor,
			Map<VariableDescriptor, Integer> binding,
			DeclarationsAccessor declarations) {
		this.sortDescriptor = sortDescriptor;
		this.binding = binding;
		this.declarations = declarations;
	}
	
	public MultisetTermEvaluator(SortDescriptor sortDescriptor) {
		this(sortDescriptor, Collections.emptyMap(), null);
	}
	
	public Multiset<Integer> evaluateMultisetTerm(Term term) {
		if (term instanceof NumberOf) {
			NumberOf numberOf = (NumberOf) term;
			Integer colorIndex = evaluateNonMultisetTerm(numberOf.getSubterm().get(1));
			int count = ((NumberConstant) numberOf.getSubterm().get(0)).getValue();
			Multiset<Integer> multiset =
					HashMultiset.create(sortDescriptor.getCardinality());
			multiset.setCount(colorIndex, count);
			return multiset;
		} else if (term instanceof All) {
			Multiset<Integer> multiset = HashMultiset
					.create(sortDescriptor.getCardinality());
			IntStream.range(0, sortDescriptor.getCardinality())
					.forEach(multiset::add);
			return multiset;
		} else if (term instanceof Empty) {
			return ImmutableMultiset.of();
		} else if (term instanceof Add) {
			Multiset<Integer> multiset = HashMultiset
					.create(sortDescriptor.getCardinality());
			for (Term subterm : ((Add) term).getSubterm()) {
				evaluateMultisetTerm(subterm).forEachEntry(multiset::add);
			}
			return multiset;
		} else if (term instanceof Subtract) {
			List<Term> subterms = ((Subtract) term).getSubterm();
			Multiset<Integer> multiset = evaluateMultisetTerm(subterms.get(0));
			evaluateMultisetTerm(subterms.get(1)).forEachEntry(multiset::remove);
			return multiset;
		} else {
			throw new UnfoldingException("Unknown multiset term: " + term);
		}		
	}
	
	private Integer evaluateNonMultisetTerm(Term term) {
		if (term instanceof Variable) {
			if (declarations == null) {
				throw new IllegalStateException("MultisetTermEvaluator for " + 
						"ground terms found an expression with variable"); 
			}
			VariableDescriptor descriptor = 
					declarations.resolveVariable((Variable) term);
			return binding.get(descriptor);
		} else if (term instanceof BuiltInConst) {
			return sortDescriptor.getIndex(term);
		} else if (term instanceof UserOperator) {
			OperatorDecl declaration = ((UserOperator) term).getDeclaration();
			if (declaration instanceof FEConstant) {
				return sortDescriptor.getIndex(declaration);
			} else {
				throw new UnfoldingException("Unknown user operator: " + term);
			}
		} else if (term instanceof BuiltInOperator) {
			return evaluateBuiltInOperator((BuiltInOperator) term);
		} else {
			throw new UnfoldingException("Unable to evaluate term: " + term);
		}
	}
	
	private Integer evaluateBuiltInOperator(BuiltInOperator op) {
		Integer left = evaluateNonMultisetTerm(op.getSubterm().get(0));
		Integer right = op.getSubterm().size() == 2 
				? evaluateNonMultisetTerm(op.getSubterm().get(1))
				: null;
		if (op instanceof BooleanOperator) {
			if (op instanceof Not) {
				return left == 0 ? 1 : 0;
			} else if (op instanceof Or) {
				return left == 1 ? 1 : right;
			} else if (op instanceof And) {
				return left == 0 ? 0 : right;
			} else if (op instanceof Imply) {
				return left == 0 
						? 1 
						: right == 1 ? 1 : 0;
			} else {
				throw new UnfoldingException("Unknown boolean operator: " + op);
			}
		} else if (op instanceof CyclicEnumOperator) {
			int upperBound = sortDescriptor.getCardinality() - 1;
			if (op instanceof Successor) {
				return left == upperBound ? 0 : left + 1;
			} else if (op instanceof Predecessor) {
				return left == 0 ? upperBound : left - 1;
			} else {
				throw new UnfoldingException("Unknown cyclic enum operator: " + op);
			}
		} else if (op instanceof FiniteIntRangeOperator) {
			if (op instanceof LessThan) {
				return left < right ? 1 : 0;
			} else if (op instanceof LessThanOrEqual) {
				return left <= right ? 1 : 0;
			} else if (op instanceof GreaterThanOrEqual) {
				return left >= right ? 1 : 0;
			} else if (op instanceof GreaterThan) {
				return left > right ? 1 : 0;
			} else {
				throw new UnfoldingException("Unknown finite int range operator: " + op);
			}
		} else if (op instanceof Equality) {
			return left.equals(right) ? 1 : 0;
		} else if (op instanceof Inequality) {
			return left.equals(right) ? 0 : 1;
		} else {
			throw new UnfoldingException("Unknown built-in operator: " + op);
		}
	}
	
}
