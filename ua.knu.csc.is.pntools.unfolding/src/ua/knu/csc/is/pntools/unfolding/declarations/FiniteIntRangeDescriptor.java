package ua.knu.csc.is.pntools.unfolding.declarations;

import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.FiniteIntConstant;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.FiniteIntRange;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.FiniteintrangesFactory;

public class FiniteIntRangeDescriptor 
		implements SortDescriptor<FiniteIntRange, FiniteIntConstant> {
	
	private FiniteIntRange sort;
	private int cardinality;
	
	public FiniteIntRangeDescriptor(FiniteIntRange sort) {
		this.sort = sort;
		this.cardinality = sort.getEnd() - sort.getStart();
	}

	@Override
	public FiniteIntRange getSort() {
		return sort;
	}

	@Override
	public int getCardinality() {
		return this.cardinality;
	}

	@Override
	public int getIndex(FiniteIntConstant constant) {
		if (!this.sort.equals(constant.getRange())) {
			throw new IllegalArgumentException("Finite int constant " +
					"from incompatible range: sort range is " + sort + 
					", constant sort range is " + constant.getRange());
		}
		return constant.getValue() - sort.getStart();
	}

	@Override
	public FiniteIntConstant getConstant(int index) {
		if (index >= cardinality) {
			throw new IndexOutOfBoundsException("Index: " + index +
					", range cardinality: " + cardinality);
		}
		FiniteIntConstant constant =
				FiniteintrangesFactory.eINSTANCE.createFiniteIntConstant();
		constant.setValue(sort.getStart() + index);
		constant.setRange(sort);
		return constant;
	}

	@Override
	public String toString(FiniteIntConstant constant) {
		return Integer.toString(constant.getValue());
	}

	@Override
	public String toString(int index) {
		if (index > cardinality) {
			throw new IndexOutOfBoundsException("Index: " + index +
					", range cardinality: " + cardinality);
		}
		return Integer.toString(sort.getStart() + index);
	}
	
	

}
