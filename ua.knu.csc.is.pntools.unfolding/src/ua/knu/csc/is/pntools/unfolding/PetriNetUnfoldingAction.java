package ua.knu.csc.is.pntools.unfolding;

import org.pnml.tools.epnk.pnmlcoremodel.PetriNet;
import org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.HLPNG;
import org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.HLPNGKind;

import ua.knu.csc.is.pntools.common.AbstractPetriNetAction;

public class PetriNetUnfoldingAction extends AbstractPetriNetAction {

	@Override
	protected PetriNetUnfoldingJob createJob() {
		return new PetriNetUnfoldingJob(this.petrinet, this.defaultInput);
	}

	@Override
	public boolean isEnabled(PetriNet petrinet) {
		return petrinet != null 
				&& petrinet.getType() instanceof HLPNG 
				&& ((HLPNG) petrinet.getType()).getKind() == HLPNGKind.SN;
	}

}
