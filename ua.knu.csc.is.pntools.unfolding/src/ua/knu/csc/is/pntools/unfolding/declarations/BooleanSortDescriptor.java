package ua.knu.csc.is.pntools.unfolding.declarations;

import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.Bool;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.BooleanConstant;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.BooleansFactory;

public class BooleanSortDescriptor implements SortDescriptor<Bool, BooleanConstant> {
	private Bool sort;
	private BooleanConstant t;
	private BooleanConstant f;
	
	public BooleanSortDescriptor(Bool sort) {
		this.sort = sort;
		this.t = BooleansFactory.eINSTANCE.createBooleanConstant();
		this.t.setValue(true);
		this.f = BooleansFactory.eINSTANCE.createBooleanConstant();
		this.f.setValue(false);
	}

	@Override
	public Bool getSort() {
		return sort;
	}

	@Override
	public int getCardinality() {
		return 2;
	}

	@Override
	public int getIndex(BooleanConstant constant) {
		return constant.isValue() ? 1 : 0;
	}

	@Override
	public BooleanConstant getConstant(int index) {
		return index == 1 
				? this.t 
				: this.f;
	}

	@Override
	public String toString(BooleanConstant constant) {
		return constant.isValue() ? "t" : "f";
	}

}
