package ua.knu.csc.is.pntools.unfolding;

public class UnfoldingException extends RuntimeException {

	public UnfoldingException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnfoldingException(String message) {
		super(message);
	}

	public UnfoldingException(Throwable cause) {
		super(cause);
	}

}
