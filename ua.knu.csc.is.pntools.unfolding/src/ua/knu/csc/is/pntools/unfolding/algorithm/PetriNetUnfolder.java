package ua.knu.csc.is.pntools.unfolding.algorithm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.graphstream.algorithm.Toolkit;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.ui.geom.Point2;
import org.graphstream.ui.geom.Point3;
import org.graphstream.ui.layout.Layout;
import org.graphstream.ui.layout.springbox.implementations.SpringBox;
import org.pnml.tools.epnk.datatypes.pnmldatatypes.NonNegativeInteger;
import org.pnml.tools.epnk.datatypes.pnmldatatypes.PositiveInteger;
import org.pnml.tools.epnk.helpers.FlatAccess;
import org.pnml.tools.epnk.pnmlcoremodel.AnnotationGraphics;
import org.pnml.tools.epnk.pnmlcoremodel.Coordinate;
import org.pnml.tools.epnk.pnmlcoremodel.Name;
import org.pnml.tools.epnk.pnmlcoremodel.NodeGraphics;
import org.pnml.tools.epnk.pnmlcoremodel.Page;
import org.pnml.tools.epnk.pnmlcoremodel.PetriNet;
import org.pnml.tools.epnk.pnmlcoremodel.PnmlcoremodelFactory;
import org.pnml.tools.epnk.pntypes.extension.PetriNetTypeExtensions;
import org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.Arc;
import org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.HLAnnotation;
import org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.HLPN;
import org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.Place;
import org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.Transition;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.Operator;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.Term;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.Variable;
import org.pnml.tools.epnk.pntypes.ptnet.PTArcAnnotation;
import org.pnml.tools.epnk.pntypes.ptnet.PTMarking;
import org.pnml.tools.epnk.pntypes.ptnet.PtnetFactory;

import ua.knu.csc.is.pntools.unfolding.UnfoldingException;
import ua.knu.csc.is.pntools.unfolding.declarations.DeclarationsAccessor;
import ua.knu.csc.is.pntools.unfolding.declarations.SortDescriptor;
import ua.knu.csc.is.pntools.unfolding.declarations.VariableDescriptor;

import com.google.common.collect.Multiset;

public class PetriNetUnfolder {
	// input SN
	private HLPN hlPetriNet;
	// utility for accessing sort and variable descriptors
	private DeclarationsAccessor declarations;
	// utility for accessing places and transitions of input SN
	private FlatAccess flatAccess;
	// number of place instances (pair <place, color> in HLPN or place in PT-net)
	private int piCount = 0;
	// index of the first place instance corresponding to the place
	Map<Place, Integer> piIndices = new HashMap<>();
	// names of place instances (in form ${placeName}_${colorName})
	private List<String> piNames = new ArrayList<>();
	// initial markings of PT-net places
	private List<Integer> piInitialMarkings = new ArrayList<>();
	// positions of place instance nodes in output graph
	private List<Point2> piPositions;
	// names of transition instances (pair <transition, binding> in HLPN
	// or transition in PT-net), in form ${transitionName}(_${varColorName})*
	private List<String> tiNames = new ArrayList<>();
	// positions of transition instance nodes in output graph
	private List<Point2> tiPositions;
	// PT-net incidence matrix (numTransitions x numPlaces)
	private List<List<Integer>> incidenceMatrix = new ArrayList<>();
	// output PT-net
	private PetriNet ptNet;
	
	public PetriNetUnfolder(HLPN petriNet) {
		this.hlPetriNet = petriNet;
	}
	
	/**
	 * Returns unfolded Petri net.
	 */
	public PetriNet getResult() {
		return ptNet;
	}
	
	/**
	 * Indicate total number of work units needed for unfolding algorithm.
	 * Helpful to track progress in job.
	 */
	public int getNumSteps() {
		return 7;
	}
	
	/**
	 * Perform 1 step of unfolding algorithm. Helpful to track progress in job.
	 * @param step 0-based step index.
	 * @return description of the next sub-task;
	 */
	public String performStep(int step) {
		switch (step) {
		case 0:
			return "Validating input Petri net...";
		case 1:
			validateInputNet();
			return "Processing declarations...";
		case 2:
			processDeclarations();
			return "Processing place instances (P/T-net places)...";
		case 3:
			initFlatAccess();
			processPlaces();
			return "Processing transition instances (P/T-net transitions)...";
		case 4:
			processTransitions();
			return "Calculating layout of output P/T net...";
		case 5:
			calculateLayout();
			return "Transforming output P/T net to PNML format...";
		case 6:
			createPTNet();
			return "";
		default:
			return null;
		}
	}
	
	private void validateInputNet() {
		Diagnostic diagnostic = Diagnostician.INSTANCE.validate(hlPetriNet);
		if (diagnostic.getCode() != Diagnostic.OK) {
			throw new UnfoldingException(
					"The supplied Petri net didn't pass validation");
		}
	}
	
	private void processDeclarations() {
		this.declarations = new DeclarationsAccessor(hlPetriNet);
	}
	
	private void initFlatAccess() {
		this.flatAccess = FlatAccess.getFlatAccess(hlPetriNet);
	}
	
	private void processPlaces() {
		List<Place> places = flatAccess.getPlaces().stream()
				.map(Place.class::cast).collect(Collectors.toList());
		for (Place place : places) {
			piIndices.put(place, piCount);
			SortDescriptor sortDescriptor = declarations.resolvePlaceSort(place);
			piCount += sortDescriptor.getCardinality();
			piNames.addAll(getPINames(place, sortDescriptor));
			piInitialMarkings.addAll(getPIInitialMarkings(place, sortDescriptor));
		}
	}
	
	private List<String> getPINames(Place place, SortDescriptor sortDescriptor) {
		String placeName = place.getName() != null && place.getName().getText() != null
				? place.getName().getText()
				: place.getId();
		int instances = sortDescriptor.getCardinality();
		List<String> names = new ArrayList<>(instances);
		for (int i = 0; i < instances; i++) {
			names.add(placeName + "_" + sortDescriptor.toString(i));
		}
		return names;
	}
	
	private List<Integer> getPIInitialMarkings(Place place, 
											   SortDescriptor sortDescriptor) {
		int instances = sortDescriptor.getCardinality();
		List<Integer> marking = new ArrayList<>(Collections.nCopies(instances, 0));
		if (place.getHlinitialMarking() != null
				&& place.getHlinitialMarking().getStructure() != null) {
			MultisetTermEvaluator evaluator = new MultisetTermEvaluator(sortDescriptor);
			Multiset<Integer> ms = evaluator
					.evaluateMultisetTerm(place.getHlinitialMarking().getStructure());
			ms.forEachEntry((index, count) -> marking.set(index, count));
		}
		return marking;
	}
	
	private void processTransitions() {
		flatAccess.getTransitions().stream()
				.map(Transition.class::cast)
				.forEach(this::processTransition);
	}
	
	private void processTransition(Transition transition) {
		List<Arc> inArcs = flatAccess.getIn(transition).stream()
				.map(Arc.class::cast).collect(Collectors.toList());
		List<Arc> outArcs = flatAccess.getOut(transition).stream()
				.map(Arc.class::cast).collect(Collectors.toList());
		List<Integer> presentVariableIndices = getPresentVariableIndices(
				Stream.concat(inArcs.stream(), outArcs.stream()));
		Term guard = transition.getCondition() != null
				? transition.getCondition().getStructure()
				: null;
		GuardCspSolver solver = 
				new GuardCspSolver(guard, declarations, presentVariableIndices);
		for (Map<VariableDescriptor, Integer> solution = solver.solve();
				solution != null;
				solution = solver.solve()) {
			tiNames.add(getTransitionInstanceName(transition, solution));
			List<Integer> incidenceMatrixRow =
					new ArrayList<>(Collections.nCopies(piCount, 0));
			for (Arc arc : inArcs) {
				evaluateArcInscription(arc, false, solution, incidenceMatrixRow);
			}
			for (Arc arc : outArcs) {
				evaluateArcInscription(arc, true, solution, incidenceMatrixRow);
			}
			incidenceMatrix.add(incidenceMatrixRow);
		}
	}
	
	private List<Integer> getPresentVariableIndices(Stream<Arc> arcs) {
		boolean present[] = new boolean[declarations.getNumVars()];
		arcs.map(Arc::getHlinscription)
			.filter(Objects::nonNull)
			.map(HLAnnotation::getStructure)
			.filter(Objects::nonNull)
			.forEach(term -> markPresentVariables(term, present));
		List<Integer> indices = new ArrayList<>();
		for (int i = 0; i < present.length; i++) {
			if (present[i]) {
				indices.add(i);
			}
		}
		return indices;
	}
	
	private void markPresentVariables(Term term, boolean[] present) {
		if (term instanceof Variable) {
			int index = declarations.resolveVariable((Variable) term).getIndex();
			present[index] = true;
		} else {
			Operator operator = (Operator) term;
			for (Term subterm : operator.getSubterm()) {
				markPresentVariables(subterm, present);
			}
		}
	}
	
	private String getTransitionInstanceName(
			Transition transition, Map<VariableDescriptor, Integer> binding) {
		StringBuilder nameBuilder;
		if (transition.getName() != null && transition.getName().getText() != null) {
			nameBuilder = new StringBuilder(transition.getName().getText());
		} else {
			nameBuilder = new StringBuilder(transition.getId());
		}
		binding.forEach((varDescriptor, index) -> {
			nameBuilder.append('_');
			nameBuilder.append(varDescriptor
					.getSortDescriptor().toString(index.intValue()));
		});
		return nameBuilder.toString();
	}
	
	private void evaluateArcInscription(Arc arc, boolean isOutArc,
			Map<VariableDescriptor, Integer> binding, 
			List<Integer> incidenceMatrixRow) {
		Place place = isOutArc 
				? (Place) arc.getTarget() 
				: (Place) arc.getSource();
		int placeOffset = piIndices.get(place);
		SortDescriptor sortDescriptor = declarations.resolvePlaceSort(place);
		MultisetTermEvaluator termEvaluator = 
				new MultisetTermEvaluator(sortDescriptor, binding, declarations);
		Term inscription = arc.getHlinscription().getStructure();
		Multiset<Integer> evaluated = termEvaluator.evaluateMultisetTerm(inscription);
		evaluated.forEachEntry((colorIndex, count) -> {
			incidenceMatrixRow.set(placeOffset + colorIndex, isOutArc ? count : -count);
		});
	}
	
	private void calculateLayout() {
		Graph graph = new MultiGraph(hlPetriNet.getId());
		for (int i = 0; i < piCount; i++) {
			graph.addNode("p" + i);
		}
		int tiCount = incidenceMatrix.size();
		for (int i = 0; i < tiCount; i++) {
			graph.addNode("t" + i);
		}
		int a = 0;
		for (int t = 0; t < tiCount; t++) {
			for (int p = 0; p < piCount; p++) {
				int weight = incidenceMatrix.get(t).get(p);
				if (weight > 0) {
					graph.addEdge("a" + a++, "t" + t, "p" + p, true);
				} else if (weight < 0) {
					graph.addEdge("a" + a++, "p" + p, "t" + t, true);
				}
			}
		}
		// calculate layout
		Layout layout = new SpringBox();
        Toolkit.computeLayout(graph, layout, 1);
        // get current low point and move all nodes so that new low point is (0,0).
        Point3 lowPoint = layout.getLowPoint();
        lowPoint.scale(-1);
        // scale all points for better visibility
        double scaleFactor = 150;
        this.piPositions = new ArrayList<>(piCount);
        for (int i = 0; i < piCount; i++) {
        	Point3 point = Toolkit.nodePointPosition(graph, "p" + i);
        	point.move(lowPoint);
        	point.scale(scaleFactor);
        	piPositions.add(point);
        }
        this.tiPositions = new ArrayList<>(tiCount);
        for (int i = 0; i < tiCount; i++) {
        	Point3 point = Toolkit.nodePointPosition(graph, "t" + i);
        	point.move(lowPoint);
        	point.scale(scaleFactor);
        	tiPositions.add(point);
        }
	}
	
	private void createPTNet() {
		// create net
		this.ptNet = PetriNetTypeExtensions.getInstance()
				.createPetriNet("http://www.pnml.org/version-2009/grammar/ptnet");
		ptNet.setId(hlPetriNet.getId() + "_unfolded");
		if (hlPetriNet.getName() != null && hlPetriNet.getName().getText() != null) {
			Name netName = PnmlcoremodelFactory.eINSTANCE.createName();
			netName.setText(hlPetriNet.getName().getText() + " Unfolded");
			ptNet.setName(netName);
		}
		
		// create page
		Page page = ptNet.getType().createPage();
		page.setId("pg1");
		Name pageName = PnmlcoremodelFactory.eINSTANCE.createName();
		pageName.setText("top-level");
		page.setName(pageName);
		ptNet.getPage().add(page);
		
		// create places
		List<org.pnml.tools.epnk.pntypes.ptnet.Place> places =
				new ArrayList<>(piCount);
		for (int i = 0; i < piCount; i++) {
			 places.add(createPlace(i));
		}
		
		// create transitions
		int tiCount = incidenceMatrix.size();
		List<org.pnml.tools.epnk.pnmlcoremodel.Transition> transitions =
				new ArrayList<>(tiCount);
		for (int i = 0; i < tiCount; i++) {
			transitions.add(createTransition(i));
		}
		
		// create arcs;
		List<org.pnml.tools.epnk.pntypes.ptnet.Arc> arcs = new ArrayList<>();
		int a = 0;
		for (int t = 0; t < tiCount; t++) {
			for (int p = 0; p < piCount; p++) {
				int weight = incidenceMatrix.get(t).get(p);
				if (weight != 0) {
					arcs.add(createArc(a++, transitions.get(t), places.get(p), weight));
				}
			}
		}
		
		// add objects to net page
		List<org.pnml.tools.epnk.pnmlcoremodel.Object> objects = page.getObject();
		objects.addAll(places);
		objects.addAll(transitions);
		objects.addAll(arcs);
	}
	
	private org.pnml.tools.epnk.pntypes.ptnet.Place createPlace(int index) {
		org.pnml.tools.epnk.pntypes.ptnet.Place place = 
				(org.pnml.tools.epnk.pntypes.ptnet.Place) ptNet.getType().createPlace();
		place.setId("p" + index);
		
		// create node graphics
		NodeGraphics nodeGraphics = PnmlcoremodelFactory.eINSTANCE.createNodeGraphics();
		Coordinate position = PnmlcoremodelFactory.eINSTANCE.createCoordinate();
		Point2 point = piPositions.get(index);
		position.setX((float) point.x);
		position.setY((float) point.y);
		nodeGraphics.setPosition(position);
		Coordinate size = PnmlcoremodelFactory.eINSTANCE.createCoordinate();
		size.setX(40);
		size.setY(40);
		nodeGraphics.setDimension(size);
		place.setGraphics(nodeGraphics);
		
		// create name label
		Name nameLabel = PnmlcoremodelFactory.eINSTANCE.createName();
		nameLabel.setText(piNames.get(index));
		AnnotationGraphics labelPos = 
				PnmlcoremodelFactory.eINSTANCE.createAnnotationGraphics();
		Coordinate offset = PnmlcoremodelFactory.eINSTANCE.createCoordinate();
		offset.setX(0);
		offset.setY(30);
		labelPos.setOffset(offset);
		nameLabel.setGraphics(labelPos);
		place.setName(nameLabel);
		
		// create initial marking label
		if (piInitialMarkings.get(index) > 0) {
			PTMarking markingLabel = PtnetFactory.eINSTANCE.createPTMarking();
			NonNegativeInteger value = 
					new NonNegativeInteger(piInitialMarkings.get(index).toString());
			markingLabel.setText(value);
			labelPos = PnmlcoremodelFactory.eINSTANCE.createAnnotationGraphics();
			offset = PnmlcoremodelFactory.eINSTANCE.createCoordinate();
			offset.setX(-8);
			offset.setY(-8);
			labelPos.setOffset(offset);
			markingLabel.setGraphics(labelPos);
			place.setInitialMarking(markingLabel);
		}
		return place;
	}
	
	private org.pnml.tools.epnk.pnmlcoremodel.Transition createTransition(int index) {
		org.pnml.tools.epnk.pnmlcoremodel.Transition transition = 
				ptNet.getType().createTransition();
		transition.setId("t" + index);
		
		// create node graphics
		NodeGraphics nodeGraphics = PnmlcoremodelFactory.eINSTANCE.createNodeGraphics();
		Coordinate position = PnmlcoremodelFactory.eINSTANCE.createCoordinate();
		Point2 point = tiPositions.get(index);
		position.setX((float) point.x);
		position.setY((float) point.y);
		nodeGraphics.setPosition(position);
		Coordinate size = PnmlcoremodelFactory.eINSTANCE.createCoordinate();
		size.setX(40);
		size.setY(40);
		nodeGraphics.setDimension(size);
		transition.setGraphics(nodeGraphics);
		
		// create name label
		Name nameLabel = PnmlcoremodelFactory.eINSTANCE.createName();
		nameLabel.setText(tiNames.get(index));
		AnnotationGraphics labelPos = 
				PnmlcoremodelFactory.eINSTANCE.createAnnotationGraphics();
		Coordinate offset = PnmlcoremodelFactory.eINSTANCE.createCoordinate();
		offset.setX(0);
		offset.setY(30);
		labelPos.setOffset(offset);
		nameLabel.setGraphics(labelPos);
		transition.setName(nameLabel);
		
		return transition;
	}
	
	private org.pnml.tools.epnk.pntypes.ptnet.Arc createArc(
			int index,
			org.pnml.tools.epnk.pnmlcoremodel.Transition transition,
			org.pnml.tools.epnk.pntypes.ptnet.Place place,
			int weight) {
		org.pnml.tools.epnk.pntypes.ptnet.Arc arc = 
				(org.pnml.tools.epnk.pntypes.ptnet.Arc) ptNet.getType().createArc();
		arc.setId("a" + index);
		arc.setSource(weight > 0 ? transition : place);
		arc.setTarget(weight > 0 ? place : transition);
		
		int absWeight = Math.abs(weight);
		if (absWeight > 1) {
			PTArcAnnotation inscription = 
					PtnetFactory.eINSTANCE.createPTArcAnnotation();
			PositiveInteger text = new PositiveInteger(absWeight + "");
			inscription.setText(text);
			AnnotationGraphics graphics = 
					PnmlcoremodelFactory.eINSTANCE.createAnnotationGraphics();
			Coordinate offset = PnmlcoremodelFactory.eINSTANCE.createCoordinate();
			offset.setX(0);
			offset.setY(30);
			graphics.setOffset(offset);
			inscription.setGraphics(graphics);
			arc.setInscription(inscription);
		}
		
		return arc;
	}
		
}
