package ua.knu.csc.is.pntools.unfolding;

import java.io.IOException;
import java.util.HashMap;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.pnml.tools.epnk.pnmlcoremodel.Object;
import org.pnml.tools.epnk.pnmlcoremodel.PetriNet;
import org.pnml.tools.epnk.pnmlcoremodel.PetriNetDoc;
import org.pnml.tools.epnk.pnmlcoremodel.PnmlcoremodelFactory;
import org.pnml.tools.epnk.pntypes.extension.PetriNetTypeExtensions;
import org.pnml.tools.epnk.pntypes.hlpng.pntd.hlpngdefinition.HLPN;

import ua.knu.csc.is.pntools.common.AbstractPetriNetJob;
import ua.knu.csc.is.pntools.unfolding.algorithm.PetriNetUnfolder;


public class PetriNetUnfoldingJob extends AbstractPetriNetJob {
	private static final String PLUGIN_ID = "ua.knu.csc.is.pntools.unfolding";

	private HLPN hlPetriNet;
	private String outputDirectory;
	private String outputFile;
	private IStatus errorStatus;

	public PetriNetUnfoldingJob(PetriNet petrinet, String outputDirectory) {
		super("pntools: Unfolding");
		this.hlPetriNet = (HLPN) EcoreUtil.copy(petrinet);
		this.outputDirectory = outputDirectory;
		this.outputFile = this.hlPetriNet.getId() + "_unfolded.pnml";
	}
	
	@Override
	public boolean prepare() {
		if (!obtainDirectory() || !obtainFileName()) {
			return false;
		}
		return true;
	}
	
	private boolean obtainDirectory() {
		ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
				shell, new WorkbenchLabelProvider(), new BaseWorkbenchContentProvider());
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		dialog.setInput(root);
		if (outputDirectory != null) {
			IProject project = root.getProject(outputDirectory);
			if (project.isOpen()) {
				dialog.setInitialSelection(project);
			}
		}
		dialog.setAllowMultiple(false);
		dialog.setTitle(getName());
		dialog.setMessage("Select project");
		dialog.setValidator(
				selection ->  selection.length == 1 && selection[0] instanceof IProject
				? new Status(IStatus.OK, PLUGIN_ID,
						"Output PNML file will be created in the selected project.") 
				: new Status(IStatus.ERROR, PLUGIN_ID,
						"Please select a project.")
				);
		if (dialog.open() == Window.OK) {
			IProject project = (IProject) dialog.getFirstResult();
			this.outputDirectory = project.getName();
			return true;
		} else {
			return false;
		}
	}
	
	private boolean obtainFileName() {
		InputDialog dlg = new InputDialog(null, getName(), "Enter file name", outputFile, 
				input -> input.endsWith(".pnml") ? null : "File name should have .pnml extension");
		if (dlg.open() == Window.OK) {
			outputFile = dlg.getValue();
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		PetriNetUnfolder unfolder = new PetriNetUnfolder(hlPetriNet);
		monitor.beginTask("Unfolding SN to P/T-net", 
				unfolder.getNumSteps() + 2); // + 2 for writing results to file and opening editor
		String nextSubTask = "Starting Petri net unfolding...";
		for (int step = 0; step < unfolder.getNumSteps(); step++) {
			if (monitor.isCanceled()) {
				return Status.CANCEL_STATUS;
			}
			monitor.subTask(nextSubTask);
			try {
				nextSubTask = unfolder.performStep(step);
			} catch (Exception e) {
				return new Status(IStatus.ERROR, PLUGIN_ID, "An error occurred while unfolding", e);
			}
			monitor.worked(1);
		}
		
		if (monitor.isCanceled()) {
			return Status.CANCEL_STATUS;
		}
		monitor.subTask("Writing results to file");
		IFile file;
		try {
			file = writeResultsToFile(unfolder.getResult());
		} catch (Exception e) {
			return new Status(IStatus.ERROR, PLUGIN_ID, "An error occurred while writing results to file", e);
		}
		monitor.worked(1);
		
		monitor.subTask("Opening output file in editor");
		openFileInEditor(file);
		monitor.worked(1);
		if (this.errorStatus != null) {
			// set from another thread
			return errorStatus;
		}
		
		monitor.done();
		showResults();
		return new Status(IStatus.OK, PLUGIN_ID, "Success!");
	}
	
	private IFile writeResultsToFile(PetriNet ptNet) throws IOException {
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(outputDirectory);
		IFile file = project.getFile(outputFile);
		final URI uri = URI.createURI(file.getFullPath().toString());
		ResourceSet resourceSet = new ResourceSetImpl();
		final Resource resource = resourceSet.createResource(uri);
		PetriNetDoc doc = PnmlcoremodelFactory.eINSTANCE.createPetriNetDoc();
		doc.getNet().add(ptNet);
		resource.getContents().add(doc);
		resource.save(new HashMap<String,Object>());
		return file;
	}
	
	
	private void openFileInEditor(IFile file) {
		shell.getDisplay().syncExec(() -> {
			IWorkbenchPage page =
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			try {
				IDE.openEditor(page, file, true);
			} catch (PartInitException e) {
				setErrorStatus(new Status(IStatus.ERROR, PLUGIN_ID, 
						"Failed to open " + outputFile + " in editor", e));
			}
		});
	}
	
	private void setErrorStatus(IStatus status) {
		this.errorStatus = status;
	}
	
	protected void doShowResults() {
		MessageDialog.openInformation(null, getName(), 
				"Unfolded Petri net has been written to file\n" + outputFile);
	}
	
	@Override
	public String getInput() {
		return this.outputDirectory;
	}

}
