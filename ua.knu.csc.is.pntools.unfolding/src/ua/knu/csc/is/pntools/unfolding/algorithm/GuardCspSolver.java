package ua.knu.csc.is.pntools.unfolding.algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.chocosolver.solver.Model;
import org.chocosolver.solver.Solver;
import org.chocosolver.solver.expression.discrete.arithmetic.ArExpression;
import org.chocosolver.solver.expression.discrete.relational.ReExpression;
import org.chocosolver.solver.variables.IntVar;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.And;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.BooleanConstant;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.BooleanOperator;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.Equality;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.Imply;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.Inequality;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.Not;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.booleans.Or;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.cyclicenumerations.CyclicEnumOperator;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.cyclicenumerations.Successor;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.dots.DotConstant;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteenumerations.FEConstant;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.FiniteIntConstant;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.FiniteIntRangeOperator;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.GreaterThan;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.GreaterThanOrEqual;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.LessThan;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.finiteintranges.LessThanOrEqual;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.BuiltInConst;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.BuiltInOperator;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.OperatorDecl;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.Term;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.UserOperator;
import org.pnml.tools.epnk.pntypes.hlpngs.datatypes.terms.Variable;

import ua.knu.csc.is.pntools.unfolding.UnfoldingException;
import ua.knu.csc.is.pntools.unfolding.declarations.DeclarationsAccessor;
import ua.knu.csc.is.pntools.unfolding.declarations.FEConstantDescriptor;
import ua.knu.csc.is.pntools.unfolding.declarations.VariableDescriptor;

public class GuardCspSolver {
	private final Term condition;
	private final DeclarationsAccessor declarations;
	private final List<Integer> variableIndicesList;
	private final Map<VariableDescriptor, Integer> variableIndices = new HashMap<>();
	private final Model model;
	private final List<IntVar> vars;
	private final Solver solver;
	
	public GuardCspSolver(Term condition, 
						  DeclarationsAccessor declarations, 
						  List<Integer> variableIndices) {
		this.condition = condition;
		this.declarations = declarations;
		this.variableIndicesList = variableIndices;
		this.model = new Model();
		this.vars = new ArrayList<>(variableIndices.size());
		this.solver = model.getSolver();
		initVars();
		postConstraints();
	}
	
	private void initVars() {
		for (Integer index : variableIndicesList) {
			VariableDescriptor descriptor = declarations.getVariableDescriptor(index);
			variableIndices.put(descriptor, index);
			int upperBound = descriptor.getSortDescriptor().getCardinality() - 1;
			vars.add(model.intVar(0, upperBound));
		}
	}
	
	private void postConstraints() {
		if (condition != null) {
			ReExpression condExpr = (ReExpression) evaluateTerm(condition);
			condExpr.post();
		}
	}
	
	private Object evaluateTerm(Term term) {
		if (term instanceof Variable) {
			return evaluateVariable((Variable) term);
		} else if (term instanceof BuiltInConst) {
			return evaluateBuiltInConst((BuiltInConst) term);
		} else if (term instanceof UserOperator) {
			return evaluateUserOperator((UserOperator) term);
		} else if (term instanceof BuiltInOperator) {
			return evaluateBuiltInOperator((BuiltInOperator) term);
		} else {
			throw new UnfoldingException("Unable to evaluate term: " + term);
		}
	}
	
	private IntVar evaluateVariable(Variable variable) {
		VariableDescriptor descriptor = declarations.resolveVariable(variable);
		int index = variableIndices.get(descriptor);
		return vars.get(index);
	}
	
	private FEConstantDescriptor evaluateUserOperator(UserOperator userOperator) {
		OperatorDecl declaration = userOperator.getDeclaration();
		if (declaration instanceof FEConstant) {
			FEConstantDescriptor descriptor = declarations
					.findFEConstant((FEConstant) declaration, variableIndicesList);
			return descriptor;
		} else {
			throw new UnfoldingException("Unable to resolve user operator,"
					+ " declaration: " + declaration);
		}
	}
	
	private IntVar evaluateBuiltInConst(BuiltInConst term) {
		if (term instanceof DotConstant) {
			return model.intVar(0);
		} else if (term instanceof BooleanConstant) {
			boolean value = ((BooleanConstant) term).isValue();
			return model.boolVar(value);
		} else if (term instanceof FiniteIntConstant) {
			FiniteIntConstant constant = (FiniteIntConstant) term;
			int index = constant.getValue() - constant.getRange().getStart();
			return model.intVar(index);
		} else {
			throw new UnfoldingException("Unknown built-in const: " + term);
		}
	}
	
	/*
	 * May return: 
	 * - ReExpression (for finite int range operator, boolean operator, eq, neq),
	 * - ArEcperession (for CyclicEnumOperator applied to Variable)
	 * - FEConstantDescriptor (for CyclicEnumOperator applied to FEConstant)
	 */
	private Object evaluateBuiltInOperator(BuiltInOperator operator) {
		if (operator instanceof FiniteIntRangeOperator) {
			return evaluateFiniteIntRangeOperator((FiniteIntRangeOperator) operator);
 		} else if (operator instanceof CyclicEnumOperator) {
			return evaluateCycliEnumOperator((CyclicEnumOperator) operator);
 		} else if (operator instanceof BooleanOperator) {
			return evaluateBooleanOperator((BooleanOperator) operator);
 		} else if (operator instanceof Equality) {
			return evaluateEquality((Equality) operator);
 		} else if (operator instanceof Inequality) {
			return evaluateInequality((Inequality) operator);
 		} else {
			throw new UnfoldingException("Unknown built-in operator: " + operator);
		}
	}
	
	private ReExpression evaluateFiniteIntRangeOperator(FiniteIntRangeOperator operator) {
		// each operand is either variable or constant (IntVar in both cases)
		IntVar left = (IntVar) evaluateTerm(operator.getSubterm().get(0));
		IntVar right = (IntVar) evaluateTerm(operator.getSubterm().get(1));
		if (operator instanceof LessThan) {
			return left.lt(right);
		} else if (operator instanceof LessThanOrEqual) {
			return left.le(right);
		} else if (operator instanceof GreaterThanOrEqual) {
			return left.ge(right);
		} else if (operator instanceof GreaterThan) {
			return left.gt(right);
		} else {
			throw new UnfoldingException("Unknown finite int range operator: " + operator);
		}
	}
	
	/*
	 * May return: 
	 * - FEConstantDescriptor (if applied to FEConstant / operator applied to constant)
	 * - ArExpression (if applied to Variable / operator applied to variable)
	 */
	private Object evaluateCycliEnumOperator(CyclicEnumOperator operator) {
		Object subterm = evaluateTerm(operator.getSubterm().get(0));
		if (subterm instanceof FEConstantDescriptor) {
			FEConstantDescriptor descriptor = (FEConstantDescriptor) subterm;
			return (operator instanceof Successor)
					? descriptor.succ()
					: descriptor.pred();
		} else {
			ArExpression expr = (ArExpression) subterm;
			int index;
			if (expr.isExpressionLeaf()) { // is variable
				index = vars.indexOf((IntVar) expr);
			} else { // is operator chain applied to variable
				HashSet<IntVar> variables = new HashSet<>();
				expr.extractVar(variables);
				index = variables.stream()
						.mapToInt(vars::indexOf)
						.filter(i -> i != -1)
						.findAny().getAsInt();
			}
			int cardinality = declarations.getVariableDescriptor(index)
					.getSortDescriptor().getCardinality();
			return (operator instanceof Successor) 
					? expr.add(1).mod(cardinality)
					: expr.add(cardinality - 1).mod(cardinality);
		}
	}
	
	private ReExpression evaluateBooleanOperator(BooleanOperator operator) {
		ReExpression left = (ReExpression) evaluateTerm(operator.getSubterm().get(0));
		ReExpression right = operator.getSubterm().size() == 2
				? (ReExpression) evaluateTerm(operator.getSubterm().get(1))
				: null;
		if (operator instanceof Not) {
			return left.not();
		} else if (operator instanceof Or) {
			return left.or(right);
		} else if (operator instanceof And) {
			return left.and(right);
		} else if (operator instanceof Imply) {
			return left.imp(right);
		} else {
			throw new UnfoldingException("Unknown boolean operator: " + operator);
		}
	}
	
	private ReExpression evaluateEquality(Equality equality) {
		Object left = evaluateTerm(equality.getSubterm().get(0));
		if (left instanceof FEConstantDescriptor) {
			left = model.intVar(((FEConstantDescriptor) left).getIndex());
		}
		Object right = evaluateTerm(equality.getSubterm().get(1));
		if (right instanceof FEConstantDescriptor) {
			right = model.intVar(((FEConstantDescriptor) right).getIndex());
		}
		// now both sides are ArExpression
		return ((ArExpression) left).eq((ArExpression) right);
	}
	
	private ReExpression evaluateInequality(Inequality inequality) {
		Object left = evaluateTerm(inequality.getSubterm().get(0));
		if (left instanceof FEConstantDescriptor) {
			left = model.intVar(((FEConstantDescriptor) left).getIndex());
		}
		Object right = evaluateTerm(inequality.getSubterm().get(1));
		if (right instanceof FEConstantDescriptor) {
			right = model.intVar(((FEConstantDescriptor) right).getIndex());
		}
		// now both sides are ArExpression
		return ((ArExpression) left).ne((ArExpression) right);
	}
	
	public Map<VariableDescriptor, Integer> solve() {
		if (solver.solve()) {
			Map<VariableDescriptor, Integer> solution = new LinkedHashMap<>();
			for (int i = 0; i < vars.size(); i++) {
				solution.put(
						declarations.getVariableDescriptor(variableIndicesList.get(i)), 
						vars.get(i).getValue());
			}
			return solution;
		} else {
			return null;
		}
	}
	
}
