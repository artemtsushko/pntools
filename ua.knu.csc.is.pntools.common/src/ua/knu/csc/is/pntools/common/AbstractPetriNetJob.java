package ua.knu.csc.is.pntools.common;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.progress.IProgressConstants;

public abstract class AbstractPetriNetJob extends Job {
	protected Shell shell = new Shell();

	public AbstractPetriNetJob(String jobName) {
		super(jobName);
		setUser(true);
	}

	public boolean prepare() {
		return true;
	}
	
	public String getInput() {
		return null;
	}
	
	protected void doShowResults() {
		// do nothing by default
	}
	
	protected final void showResults() {
		if (isModal()) {
			shell.getDisplay().asyncExec(() -> doShowResults());
		} else {
			setProperty(IProgressConstants.KEEP_PROPERTY, Boolean.TRUE);
			setProperty(IProgressConstants.ACTION_PROPERTY, new Action("View results") {
				@Override
				public void run() {
					doShowResults();
				}
			});
		}
	}
	
	protected final boolean isModal() {
        Boolean isModal = (Boolean) this.getProperty(
                               IProgressConstants.PROPERTY_IN_DIALOG);
        if(isModal == null) return false;
        return isModal.booleanValue();
     }

}
