package ua.knu.csc.is.pntools.common;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.pnml.tools.epnk.pnmlcoremodel.PetriNet;

public abstract class AbstractPetriNetAction implements IObjectActionDelegate {
	protected PetriNet petrinet;
	protected String defaultInput;
	
	public boolean isEnabled(PetriNet petrinet) {
		return true;
	}
	
	protected abstract AbstractPetriNetJob createJob();

	@Override
	public void run(IAction action) {
		AbstractPetriNetJob job = createJob();
		if (job.prepare()) {
			defaultInput = job.getInput();
			job.setPriority(Job.SHORT);
			job.schedule(); 
		}
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		petrinet = null;
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			if (structuredSelection.size() == 1
					&& structuredSelection.getFirstElement() instanceof PetriNet) {
				petrinet = (PetriNet) structuredSelection
						.getFirstElement();
			}
		}
		action.setEnabled(isEnabled(petrinet));
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// Auto-generated method stub
	}

}
