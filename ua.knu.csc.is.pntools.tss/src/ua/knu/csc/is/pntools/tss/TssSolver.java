package ua.knu.csc.is.pntools.tss;


public class TssSolver {
	private int[][] equations;
	private int dimensions;
	private int[][] solutions;
	private int sign;
	
	public TssSolver(int[][] equations, int sign) {
		validateEquations(equations);
		this.equations = equations;
		this.dimensions = equations[0].length;
		this.solutions = generateBasisVectors(this.dimensions);
		this.sign = sign;
	}
	
	private static void validateEquations(int[][] equations) {
		if (equations.length == 0) {
			throw new IllegalArgumentException(
					"System of equations (inequalities) is empty");
		}
		int dim = equations[0].length;
		if (dim == 0) {
			throw new IllegalArgumentException(
					"Equation (inequality) doesn't contain any coefficient (variable)");
		}
		for (int i = 1; i < equations.length; i++) {
			if (equations[i].length != dim) {
				throw new IllegalArgumentException(
						"All equations (inequalities) should have "
						+ "equal number of coefficients (variables)");
			}
		}
	}
	
	/**
	 * Generates vectors of the canonical basis.
	 */
	private static int[][] generateBasisVectors(int dimensions) {
		int[][] basis = new int[dimensions][];
		for (int i = 0; i < dimensions; i++) {
			basis[i] = new int[dimensions];
			for (int j = 0; j < dimensions; j++) {
				basis[i][j] = j == i ? 1 : 0;
			}
		}
		return basis;
	}
	
	/**
	 * Solves system of equations (inequalities) using TSS algorithm.
	 */
	public int[][] solve() {
		for (int i = 0; i < equations.length && solutions.length > 0; i++) {
			generateNextSolutions(i);
		}
		return solutions;
	}
	
	/**
	 * Builds TSS for equations (inequalities) [0...equationIndex] provided that
	 * <code>this.solutions</code> is TSS for equations (inequalities) [0...(equationIndex - 1)].
	 */
	private void generateNextSolutions(int equationIndex) {
		int[] equation = this.equations[equationIndex];
		// results of substituting solutions to left part of equation (inequality)
		int[] results = new int[this.solutions.length]; 
		int numPositive = 0; // number of positive results
		int numNegative = 0; // number of negative results
		int numZero = 0;	 // number of zero results
		
		// calculate results for each vector from previous TSS
		for (int i = 0; i < this.solutions.length; i++) {
			int result = 0;
			for (int j = 0; j < this.dimensions; j++) {
				result = Math.addExact(result, 
						Math.multiplyExact(equation[j], this.solutions[i][j]));
			}
			results[i] = result;
			if (result > 0) {
				numPositive++;
			} else if (result < 0) {
				numNegative++;
			} else {
				numZero++;
			}
		}
		
		/* return immediately if next TSS is going to be empty, that is:
		 	* M0 is empty and M+ or M- is empty in case of == equations 
		 	* M0 is empty and M+ is empty in case of >= inequalities
		 	* M0 is empty and M- is empty in case of <= inequalities
		 */
		if (numZero == 0 && (sign >= 0 && numPositive == 0 || sign <= 0 && numNegative == 0)) {
			this.solutions = new int[0][];
			return;
		}
		
		// partition vectors of previous TSS into M+, M- and M0 sets
		int[] posIndices = new int[numPositive];
		int[] negIndices = new int[numNegative];
		int[] zeroIndices = new int[numZero];
		partitionResultIndices(results, posIndices, negIndices, zeroIndices);
		
		// initialize array for next TSS
		int nextSolutionsSize = numZero + numPositive * numNegative;
		if (sign > 0) {
			nextSolutionsSize += numPositive;
		} else if (sign < 0) {
			nextSolutionsSize += numNegative;
		}
		int [][] nextSolutions = new int[nextSolutionsSize][];
		
		
		int s = 0; // next solution index;
		// add vectors from M0 to next TSS
		for (int i = 0; i < numZero; i++) {
			nextSolutions[s++] = this.solutions[zeroIndices[i]];
		}
		if (sign > 0) {
			// for system of >= inequalities, also add vectors from M+ to next TSS
			for (int i = 0; i < numPositive; i++) {
				nextSolutions[s++] = this.solutions[posIndices[i]];
			}
		} else if (sign < 0) {
			// for system of <= inequalities, also add vectors from M- to next TSS
			for (int i = 0; i < numNegative; i++) {
				nextSolutions[s++] = this.solutions[negIndices[i]];
			}
		}
		// add combinations of vectors from M+ and M-
		for (int p = 0; p < numPositive; p++) {
			for (int n = 0; n < numNegative; n++) {
				int[] posSolution = this.solutions[posIndices[p]];
				int[] negSolution = this.solutions[negIndices[n]];
				int posCoef = -1 * results[negIndices[n]];
				int negCoef = results[posIndices[p]];
				int[] solution = new int[this.dimensions];
				for (int i = 0; i < this.dimensions; i++) {
					solution[i] = Math.addExact(
							Math.multiplyExact(posCoef, posSolution[i]),
							Math.multiplyExact(negCoef, negSolution[i]));
				}
				shortenByGCD(solution);
				nextSolutions[s++] = solution;
			}
		}
		
		// in case of equations, remove redundant solutions (that is, vectors x
		// such that t * x >= y, for any other y from TSS and for some scalar t)
		this.solutions = sign == 0
				? removeRedundantSolutions(nextSolutions)
				: nextSolutions;
	}
	
	private static void partitionResultIndices(int[] results, 
			int[] posIndices, int[] negIndices, int[] zeroIndices) {
		int p = 0;
		int n = 0;
		int z = 0;
		for (int i = 0; i < results.length; i++) {
			if (results[i] > 0) {
				posIndices[p++] = i;
			} else if (results[i] < 0) {
				negIndices[n++] = i;
			} else {
				zeroIndices[z++] = i;
			}
		}
	}
	
	private static void shortenByGCD(int[] solution) {
		int gcd = GCD.absGCD(solution);
		if (gcd > 1) {
			for (int i = 0; i < solution.length; i++)  {
				solution[i] /= gcd;
			}
		}
	}
	
	private static int[][] removeRedundantSolutions(int[][] allSolutions) {
		int maxCoef = Integer.MIN_VALUE;
		for (int[] solution : allSolutions) {
			for (int coef: solution) {
				if (coef > maxCoef) {
					maxCoef = coef;
				}
			}
		}
		
		int numRedundant = 0;
		boolean isRedundant[] = new boolean[allSolutions.length];
		for (int i = 0; i < allSolutions.length; i++) {
			int[] solution = allSolutions[i];
			int[] amplified = new int[solution.length];
			for (int k = 0; k < solution.length; k++) {
				amplified[k] = maxCoef * solution[k];
			}
			
			isRedundant[i] = false;
			for (int j = 0; j < allSolutions.length; j++) {
				if (j == i || isRedundant[j]) {
					continue;
				}
				boolean gte = true;
				for (int k = 0; k < amplified.length; k++) {
					if (amplified[k] < allSolutions[j][k]) {
						gte = false;
						break;
					}
				}
				if (gte) {
					isRedundant[i] = true;
					numRedundant++;
					break;
				}
			}
		}
		
		if (numRedundant != 0) {
			int[][] minSolutions = new int[allSolutions.length - numRedundant][];
			int s = 0;
			for (int i = 0; i < allSolutions.length; i++) {
				if (!isRedundant[i]) {
					minSolutions[s++] = allSolutions[i];
				}
			}
			return minSolutions;
		} else {
			return allSolutions;
		}
	}
	
	/*
	public static void main(String[] args) {
		int[][] solutions = new TssSolver(TEST_SET_3, 1).solve();
		if (solutions.length == 0) {
			System.out.print("System of equations doesn't have solutions");
		}
		for (int[] solution : solutions) {
			System.out.print("[" + solution[0]);
			for (int i = 1; i < solution.length; i++) {
				System.out.print(", " + solution[i]);
			}
			System.out.println("]");
		}
	}
	
	private static int[][] TEST_SET_1 = {
		{ 4, 2, -3, -2, -1 },
		{ 2, -1, -4, 1, 5 },
		{ 0, 2, 4, 0, -9 }
	};
	
	private static int[][] TEST_SET_2 = {
		{ 5, 0, 3, -4, 0, 0 },
		{ 1, 2, 1, 0, -3, 0 },
		{ 0, 1, 2, 0, 0, -2 }
	};
	
	private static int[][] TEST_SET_3 = {
		{ -3, -4, 5, -6 },
		{ 2, 3, -3, 1 }
	};
	*/

}
