package ua.knu.csc.is.pntools.tss;


public final class GCD {
	
	private GCD() {}
	
	public static int gcd(int a, int b) {
		if (a == 0) {
	      return b;
	    } else if (b == 0) {
	      return a;
	    }

		// Binary GCD algorithm
	    int aTwos = Integer.numberOfTrailingZeros(a);
	    a >>= aTwos;
	    int bTwos = Integer.numberOfTrailingZeros(b);
	    b >>= bTwos; 
	    while (a != b) {
	    	int newB = Math.min(a, b);
	    	a = Math.abs(a - b);
	    	a >>= Integer.numberOfTrailingZeros(a);
	    	b = newB;
	    }
	    return a << Math.min(aTwos, bTwos);
	}
	
	public static int gcd(int[] numbers) {
		return gcd(numbers, 0, numbers.length - 1);
	}
	
	private static int gcd(int[] numbers, int p, int q) {
		if (p == q) {
			return numbers[p];
		} else {
			int mid = (p + q) / 2;
			int left = gcd(numbers, p, mid);
			int right = gcd(numbers, mid + 1, q);
			return gcd(left,right);
		}
	}
	
	public static int absGCD(int a, int b) {
		return gcd(Math.abs(a), Math.abs(b));
	}
	
	public static int absGCD(int[] numbers) {
		return absGCD(numbers, 0, numbers.length - 1);
	}
	
	private static int absGCD(int[] numbers, int p, int q) {
		if (p == q) {
			return numbers[p];
		} else {
			int mid = (p + q) / 2;
			int left = absGCD(numbers, p, mid);
			int right = absGCD(numbers, mid + 1, q);
			return absGCD(left,right);
		}
	}
	
}
