package ua.knu.csc.is.pntools.analysis.ptnets;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.pnml.tools.epnk.pnmlcoremodel.PetriNet;

public class PTNetStructuralPropertiesAnalysisJob extends Job {
	private static final String PLUGIN_ID = "ua.knu.csc.is.pntools.analysis";
	
	private PetriNet petriNet;
	private PTNetStructuralProperties structuralProperties;

	public PTNetStructuralPropertiesAnalysisJob(PetriNet petriNet) {
		super("pntools: P/T-net structural properties analysis");
		this.petriNet = petriNet;
	}
	
	public PTNetStructuralProperties getStructuralProperties() {
		return structuralProperties;
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		this.structuralProperties = new PTNetStructuralProperties();
		monitor.beginTask("Calculating P/T-net structural properties", 14);
		
		if (!monitor.isCanceled()) {
			monitor.subTask("Checking if P/T-net is pure");
			boolean pure = PTNetStructuralPropertiesAnalyzer.isPure(petriNet);
			structuralProperties.setPure(pure);
			monitor.worked(1);
			if (!pure) {
				return new Status(IStatus.OK, PLUGIN_ID, "Done!");
			}
		} else {
			return Status.CANCEL_STATUS;
		}
		
		if (!monitor.isCanceled()) {
			monitor.subTask("Building internal representation" + 
					" (incidence matrix and initial marking vector)");
			PTNetStructuralPropertiesAnalyzer
					.buildInternalRepresentation(petriNet, structuralProperties);
			monitor.worked(2);
		} else {
			return Status.CANCEL_STATUS;
		}
		
		if (!monitor.isCanceled()) {
			monitor.subTask("Calculting S-invariants for P/T-net");
			PTNetStructuralPropertiesAnalyzer
					.findSInvariants(structuralProperties);
			monitor.worked(2);
		} else {
			return Status.CANCEL_STATUS;
		}
		
		if (!monitor.isCanceled()) {
			monitor.subTask("Computing place boundaries");
			PTNetStructuralPropertiesAnalyzer
					.computePlaceBoundaries(structuralProperties);
			monitor.worked(1);
		} else {
			return Status.CANCEL_STATUS;
		}
		
		if (!monitor.isCanceled()) {
			monitor.subTask("Checking if P/T-net is (partially) conservative");
			PTNetStructuralPropertiesAnalyzer
					.checkIfConservative(structuralProperties);
			monitor.worked(1);
		} else {
			return Status.CANCEL_STATUS;
		}
		
		if (!monitor.isCanceled()) {
			monitor.subTask("Checking if P/T-net is bounded");
			PTNetStructuralPropertiesAnalyzer
					.checkIfStructurallyBounded(structuralProperties);
			monitor.worked(2);
		} else {
			return Status.CANCEL_STATUS;
		}
		
		if (!monitor.isCanceled()) {
			monitor.subTask("Calculting T-invariants for P/T-net");
			PTNetStructuralPropertiesAnalyzer
					.findTInvariants(structuralProperties);
			monitor.worked(2);
		} else {
			return Status.CANCEL_STATUS;
		}
		
		if (!monitor.isCanceled()) {
			monitor.subTask("Checking if P/T-net is (partially) consistent");
			PTNetStructuralPropertiesAnalyzer
					.checkIfConsistent(structuralProperties);
			monitor.worked(1);
		} else {
			return Status.CANCEL_STATUS;
		}
		
		if (!monitor.isCanceled()) {
			monitor.subTask("Checking if P/T-net is (partially) repetitive");
			PTNetStructuralPropertiesAnalyzer
					.checkIfRepetitive(structuralProperties);
			monitor.worked(2);
		} else {
			return Status.CANCEL_STATUS;
		}
		
		monitor.done();
		return new Status(IStatus.OK, PLUGIN_ID, "Done!");
	}

}
