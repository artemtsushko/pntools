package ua.knu.csc.is.pntools.analysis.ptnets;

public class PTNetStructuralProperties {
	private Boolean pure;
	private int[][] incidenceMatrix;
	private int[][] transposedIncidenceMatrix;
	private int[]	initialMarking;
	private String[] placeLabels;
	private String[] transitionLabels;
	private Boolean structurallyBounded;
	private Boolean conservative;
	private Boolean partiallyConservative;
	private Boolean repetitive;
	private Boolean partiallyRepetitive;
	private Boolean consistent;
	private Boolean partiallyConsistent;
	private int[][] sInvariants;
	private Integer[] placeBoundaries;
	private int[][] tInvariants;
	
	
	public Boolean isPure() {
		return pure;
	}
	public void setPure(Boolean pure) {
		this.pure = pure;
	}
	public int[][] getIncidenceMatrix() {
		return incidenceMatrix;
	}
	public void setIncidenceMatrix(int[][] incidenceMatrix) {
		this.incidenceMatrix = incidenceMatrix;
	}
	public int[][] getTransposedIncidenceMatrix() {
		return transposedIncidenceMatrix;
	}
	public void setTransposedIncidenceMatrix(int[][] transposedIncidenceMatrix) {
		this.transposedIncidenceMatrix = transposedIncidenceMatrix;
	}
	public int[] getInitialMarking() {
		return initialMarking;
	}
	public void setInitialMarking(int[] initialMarking) {
		this.initialMarking = initialMarking;
	}
	public String[] getPlaceLabels() {
		return placeLabels;
	}
	public void setPlaceLabels(String[] placeLabels) {
		this.placeLabels = placeLabels;
	}
	public String[] getTransitionLabels() {
		return transitionLabels;
	}
	public void setTransitionLabels(String[] transitionLabels) {
		this.transitionLabels = transitionLabels;
	}
	public Boolean isStructurallyBounded() {
		return structurallyBounded;
	}
	public void setStructurallyBounded(Boolean structurallyBounded) {
		this.structurallyBounded = structurallyBounded;
	}
	public Boolean isConservative() {
		return conservative;
	}
	public void setConservative(Boolean conservative) {
		this.conservative = conservative;
	}
	public Boolean isPartiallyConservative() {
		return partiallyConservative;
	}
	public void setPartiallyConservative(Boolean partiallyConservative) {
		this.partiallyConservative = partiallyConservative;
	}
	public Boolean isRepetitive() {
		return repetitive;
	}
	public void setRepetitive(Boolean repetitive) {
		this.repetitive = repetitive;
	}
	public Boolean isPartiallyRepetitive() {
		return partiallyRepetitive;
	}
	public void setPartiallyRepetitive(Boolean partiallyRepetitive) {
		this.partiallyRepetitive = partiallyRepetitive;
	}
	public Boolean isConsistent() {
		return consistent;
	}
	public void setConsistent(Boolean consistent) {
		this.consistent = consistent;
	}
	public Boolean isPartiallyConsistent() {
		return partiallyConsistent;
	}
	public void setPartiallyConsistent(Boolean partiallyConsistent) {
		this.partiallyConsistent = partiallyConsistent;
	}
	public int[][] getSInvariants() {
		return sInvariants;
	}
	public void setSInvariants(int[][] sInvariants) {
		this.sInvariants = sInvariants;
	}
	public Integer[] getPlaceBoundaries() {
		return placeBoundaries;
	}
	public void setPlaceBoundaries(Integer[] placeBoundaries) {
		this.placeBoundaries = placeBoundaries;
	}
	public int[][] getTInvariants() {
		return tInvariants;
	}
	public void setTInvariants(int[][] tInvariants) {
		this.tInvariants = tInvariants;
	}
	
	
}
