package ua.knu.csc.is.pntools.analysis.views;

import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.IJobChangeListener;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.ExpandBar;
import org.eclipse.swt.widgets.ExpandItem;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.part.ViewPart;
import org.pnml.tools.epnk.pnmlcoremodel.PetriNet;
import org.pnml.tools.epnk.pntypes.ptnet.PTNet;

import ua.knu.csc.is.pntools.analysis.ptnets.PTNetStructuralProperties;
import ua.knu.csc.is.pntools.analysis.ptnets.PTNetStructuralPropertiesAnalysisJob;

public class PTNetStructuralPropertiesView extends ViewPart implements ISelectionListener {
	private Composite parent;
	private Control control;
	private PTNetStructuralPropertiesAnalysisJob job;
	private IJobChangeListener jobChangeListener;
	
	public PTNetStructuralPropertiesView() {
		super();
	}

	@Override
	public void createPartControl(Composite parent) {
		this.parent = parent;
		
		Label label = new Label(parent, SWT.NONE);
		label.setText("Select a P/T-net");
		this.control = label;
		
        getViewSite().getPage().addSelectionListener(this);
        selectionChanged(null, getSite().getPage().getSelection());
	}
	
	public void dispose() {
        super.dispose();
        getSite().getPage().removeSelectionListener(this);
        cancelCurrentlyRunningJob();
    }

	@Override
	public void setFocus() {
		control.setFocus();
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		if (selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			if (structuredSelection.size() == 1
					&& structuredSelection.getFirstElement() instanceof PetriNet) {
				PetriNet petriNet = (PetriNet) structuredSelection.getFirstElement();
				if (petriNet.getType() != null && petriNet.getType() instanceof PTNet) {
					cancelCurrentlyRunningJob();
					job = new PTNetStructuralPropertiesAnalysisJob(petriNet);
					job.setPriority(Job.SHORT);
					jobChangeListener = new JobChangeAdapter() {
						@Override
						public void done(IJobChangeEvent event) {
							if (event.getResult().isOK() && event.getJob() == job
									&& job.getStructuralProperties() != null
									&& !parent.isDisposed()) {
								parent.getDisplay().asyncExec(() -> {
									updateUI(job.getStructuralProperties());
								});
							}
						}
					};
					job.addJobChangeListener(jobChangeListener);
					job.schedule();
				}
			}
		}
	}
	
	private void cancelCurrentlyRunningJob() {
		if (job != null) {
        	if (jobChangeListener != null) {
        		job.removeJobChangeListener(jobChangeListener);
        	}
        	job.cancel();
        }
	}
	
	private void updateUI(PTNetStructuralProperties newStructuralProperties) {		
		control.setVisible(false);
		control.dispose();
		if (newStructuralProperties.isPure() == null || !newStructuralProperties.isPure()) {
			Label label = new Label(parent, SWT.NONE);
			label.setText("Cannot compute structural properties:\nthe P/T-net is not pure");
			this.control = label;
		} else {
			this.control = createUIForStructuralProperties(newStructuralProperties);
		}
		parent.layout(true, true);
	}
	
	private ExpandBar createUIForStructuralProperties(PTNetStructuralProperties structProps) {
		ExpandBar expandBar = new ExpandBar(parent, SWT.H_SCROLL | SWT.V_SCROLL);
		
		ExpandItem summary = new ExpandItem(expandBar, SWT.NONE);
		summary.setText("Summary");
		Table summaryTable = createSummaryTable(structProps, expandBar);
		summary.setHeight(summaryTable.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		summary.setControl(summaryTable);
		
		ExpandItem sInvariants = new ExpandItem(expandBar, SWT.NONE);
		sInvariants.setText("S-invariants");
		Table sInvariantsTable = createSInvariantsTable(structProps, expandBar);
		sInvariants.setHeight(sInvariantsTable.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		sInvariants.setControl(sInvariantsTable);
		
		ExpandItem tInvariants = new ExpandItem(expandBar, SWT.NONE);
		tInvariants.setText("T-invariants");
		Table tInvariantsTable = createTInvariantsTable(structProps, expandBar);
		tInvariants.setHeight(tInvariantsTable.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
		tInvariants.setControl(tInvariantsTable);
		
		if (structProps.getSInvariants().length > 0) {
			ExpandItem placeBoundaries = new ExpandItem(expandBar, SWT.NONE);
			placeBoundaries.setText("Place Boundaries");
			Table placeBoundariesTable = createPlaceBoundariesTable(structProps, expandBar);
			placeBoundaries.setHeight(placeBoundariesTable.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
			placeBoundaries.setControl(placeBoundariesTable);
		}
		
		return expandBar;
	}
	
	private Table createSummaryTable(PTNetStructuralProperties structProps, Composite composite) {
		Table table = new Table(composite, SWT.NONE);
		
		TableColumn propertyName = new TableColumn(table, SWT.LEFT);
		propertyName.setText("Property Name");
		propertyName.setWidth(192);
		
		TableColumn propertyValue = new TableColumn(table, SWT.LEFT);
		propertyValue.setText("Property Value");
		propertyValue.setWidth(32);
		
		createSummaryTableItem(table, "Structurally Bounded", structProps.isStructurallyBounded());
		createSummaryTableItem(table, "Conservative", structProps.isConservative());
		createSummaryTableItem(table, "Partially Conservative", structProps.isPartiallyConservative());
		createSummaryTableItem(table, "Repetitive", structProps.isRepetitive());
		createSummaryTableItem(table, "Partially Repetitive", structProps.isPartiallyRepetitive());
		createSummaryTableItem(table, "Consistent", structProps.isConsistent());
		createSummaryTableItem(table, "Partially Consistent", structProps.isPartiallyConsistent());
		createSummaryTableItem(table, "Pure", structProps.isPure());
		
		TableItem item = new TableItem(table, SWT.NONE);
		item.setText(0, "Places");
		item.setText(1, structProps.getPlaceLabels() == null 
				? "" 
				: structProps.getPlaceLabels().length + "");
		
		item = new TableItem(table, SWT.NONE);
		item.setText(0, "Transitions");
		item.setText(1, structProps.getTransitionLabels() == null 
				? "" 
				: structProps.getTransitionLabels().length + "");
		
		return table;
	}
	
	private void createSummaryTableItem(Table table, String name, Boolean value) {
		TableItem item = new TableItem(table, SWT.NONE);
		item.setText(0, name);
		item.setText(1, value == null 
				? "" 
				: value ? "+" : "-");
	}
	
	private Table createSInvariantsTable(PTNetStructuralProperties structProps, Composite composite) {
		Table table = new Table(composite, SWT.NONE);
		
		TableColumn place = new TableColumn(table, SWT.LEFT);
		place.setText("Place");
		place.setWidth(96);
		
		int[][] sInvariants = structProps.getSInvariants();
		for (int i = 0; i < sInvariants.length; i++) {
			TableColumn inv = new TableColumn(table, SWT.LEFT);
			inv.setText("Invariant " + i);
			inv.setWidth(32);
		}
		
		String[] placeLabels = structProps.getPlaceLabels();
		for (int p = 0; p < placeLabels.length; p++) {
			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(0, placeLabels[p]);
			for (int k = 0; k < sInvariants.length; k++) {
				item.setText(k + 1, sInvariants[k][p] + "");
			}
		}
		
		place.pack();
		
		return table;
	}
	
	private Table createPlaceBoundariesTable(PTNetStructuralProperties structProps, Composite composite) {
		Table table = new Table(composite, SWT.NONE);
		
		TableColumn place = new TableColumn(table, SWT.LEFT);
		place.setText("Place");
		place.setWidth(96);
		
		TableColumn boundary = new TableColumn(table, SWT.LEFT);
		boundary.setText("Boundary");
		boundary.setWidth(32);
		
		Integer[] boundaries = structProps.getPlaceBoundaries();
		String[] placeLabels = structProps.getPlaceLabels();
		for (int p = 0; p < placeLabels.length; p++) {
			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(0, placeLabels[p]);
			item.setText(1, boundaries[p] == null 
					? "\u03c9" // small Greek letter Omega
					: boundaries[p].toString());
		}
		
		place.pack();
		
		return table;
	}
	
	private Table createTInvariantsTable(PTNetStructuralProperties structProps, Composite composite) {
		Table table = new Table(composite, SWT.NONE);
		
		TableColumn transition = new TableColumn(table, SWT.LEFT);
		transition.setText("Transition");
		transition.setWidth(96);
		
		int[][] tInvariants = structProps.getTInvariants();
		for (int i = 0; i < tInvariants.length; i++) {
			TableColumn inv = new TableColumn(table, SWT.LEFT);
			inv.setText("Invariant " + i);
			inv.setWidth(32);
		}
		
		String[] transitionLabels = structProps.getTransitionLabels();
		for (int t = 0; t < transitionLabels.length; t++) {
			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(0, transitionLabels[t]);
			for (int k = 0; k < tInvariants.length; k++) {
				item.setText(k + 1, tInvariants[k][t] + "");
			}
		}
		
		transition.pack();
		
		return table;
	}
	
}
