package ua.knu.csc.is.pntools.analysis.ptnets;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

import org.pnml.tools.epnk.helpers.FlatAccess;
import org.pnml.tools.epnk.pnmlcoremodel.Arc;
import org.pnml.tools.epnk.pnmlcoremodel.PetriNet;
import org.pnml.tools.epnk.pnmlcoremodel.Place;
import org.pnml.tools.epnk.pnmlcoremodel.PlaceNode;
import org.pnml.tools.epnk.pnmlcoremodel.Transition;
import org.pnml.tools.epnk.pntypes.ptnet.PTArcAnnotation;
import org.pnml.tools.epnk.pntypes.ptnet.PTMarking;

import ua.knu.csc.is.pntools.tss.TssSolver;

public class PTNetStructuralPropertiesAnalyzer {
	
	private PTNetStructuralPropertiesAnalyzer() {}
	
	public static boolean isPure(PetriNet petriNet) {
		FlatAccess flat = FlatAccess.getFlatAccess(petriNet);
		List<Transition> transitions = flat.getTransitions();
		for (Transition t: transitions) {
			Set<Place> inputPlaces = new HashSet<>();
			for (Arc arc : flat.getIn(t)) {
				Place p = flat.resolve((PlaceNode) arc.getSource());
				inputPlaces.add(p);
			}
			for (Arc arc : flat.getOut(t)) {
				Place p = flat.resolve((PlaceNode) arc.getTarget());
				if (inputPlaces.contains(p)) {
					return false;
				}
			}
		}
		return true;
	}
	
	/**
	 * Computes incidence matrix, transposed incidence matrix, initial marking vector,
	 * vectors of labels for places and transitions (either name or id).
	 */
	public static void buildInternalRepresentation(PetriNet petriNet,
												   PTNetStructuralProperties properties) {
		FlatAccess flat = FlatAccess.getFlatAccess(petriNet);
		List<Place> places = flat.getPlaces();
		int[] initialMarkings = new int[places.size()];
		Map<Place, Integer> placeIndices = new HashMap<>();
		for (ListIterator<Place> it = places.listIterator(); it.hasNext(); ) {
			Place p = it.next();
			placeIndices.put(p, it.previousIndex());
			PTMarking initialMarking = 
					((org.pnml.tools.epnk.pntypes.ptnet.Place) p).getInitialMarking();
			initialMarkings[it.previousIndex()] = 
					initialMarking != null && initialMarking.getText() != null
							? initialMarking.getText().getValue()
							: 0;
		}
		properties.setInitialMarking(initialMarkings);
		
		List<Transition> transitions = flat.getTransitions();
		int[][] incidenceMatrix = new int[transitions.size()][];
		for (int tIndex = 0; tIndex < transitions.size(); tIndex++) {
			Transition t = transitions.get(tIndex);
			incidenceMatrix[tIndex] = new int[places.size()];
			for (Arc arc : flat.getIn(t)) {
				Place p = flat.resolve((PlaceNode) arc.getSource());
				PTArcAnnotation inscription = 
						((org.pnml.tools.epnk.pntypes.ptnet.Arc) arc).getInscription();
				int weight = inscription != null && inscription.getText() != null
						? inscription.getText().getValue()
						: 1;
				int pIndex = placeIndices.get(p);
				incidenceMatrix[tIndex][pIndex] = 
						Math.subtractExact(incidenceMatrix[tIndex][pIndex], weight);
				
			}
			for (Arc arc : flat.getOut(t)) {
				Place p = flat.resolve((PlaceNode) arc.getTarget());
				PTArcAnnotation inscription = 
						((org.pnml.tools.epnk.pntypes.ptnet.Arc) arc).getInscription();
				int weight = inscription != null && inscription.getText() != null
						? inscription.getText().getValue()
						: 1;
				int pIndex = placeIndices.get(p);
				incidenceMatrix[tIndex][pIndex] = 
						Math.addExact(incidenceMatrix[tIndex][pIndex], weight);
			}
		}
		properties.setIncidenceMatrix(incidenceMatrix);
		properties.setTransposedIncidenceMatrix(transposeMatrix(incidenceMatrix));
		
		String[] placeLabels = places.stream()
				.map(p -> p.getName() != null 
						? p.getName().getText() 
						: p.getId() + " (id)")
				.toArray(String[]::new);
		properties.setPlaceLabels(placeLabels);
		
		String[] transitionLabels = transitions.stream()
				.map(t -> t.getName() != null 
						? t.getName().getText() 
						: t.getId() + " (id)")
				.toArray(String[]::new);
		properties.setTransitionLabels(transitionLabels);
	}
	
	private static int[][] transposeMatrix(int[][] matrix) {
		int cols = matrix.length;
		int rows = cols != 0 ? matrix[0].length : 0;
		int[][] transposed = new int[rows][];
		for (int i = 0; i < rows; i++) {
			transposed[i] = new int[cols];
			for (int j = 0; j < cols; j++) {
				transposed[i][j] = matrix[j][i];
			}
		}
		return transposed;
	}
	
	public static void findSInvariants(PTNetStructuralProperties properties) {
		if (properties.getIncidenceMatrix().length > 0) {
			TssSolver tssSolver = new TssSolver(properties.getIncidenceMatrix(), 0);
			int[][] tss = tssSolver.solve();
			properties.setSInvariants(tss);
		} else {
			properties.setSInvariants(new int[0][]);
		}
	}
	
	public static void computePlaceBoundaries(PTNetStructuralProperties properties) {
		int[] initialMarking = properties.getInitialMarking();
		int[][] sInvariants = properties.getSInvariants();
		
		int[] products = new int[sInvariants.length];
		for (int i = 0; i < sInvariants.length; i++) {
			products[i] = 0;
			for (int j = 0; j < initialMarking.length; j++) {
				products[i] = Math.addExact(products[i], 
						Math.multiplyExact(initialMarking[j], sInvariants[i][j]));
			}
		}
		
		Integer[] boundaries = new Integer[initialMarking.length];
		for (int p = 0; p < boundaries.length; p++) {
			for (int i = 0; i < sInvariants.length; i++) {
				if (sInvariants[i][p] != 0) {
					int fraction = products[i] / sInvariants[i][p];
					if (boundaries[p] == null || fraction < boundaries[p]) {
						boundaries[p] = fraction;
					}
				}
			}
		}
		properties.setPlaceBoundaries(boundaries);
	}
	
	public static void checkIfConservative(PTNetStructuralProperties properties) {
		if (properties.getIncidenceMatrix().length > 0) {
			int[][] sInvariants = properties.getSInvariants();
			properties.setConservative(positiveLinearCombinationExists(sInvariants));
			properties.setPartiallyConservative(properties.isConservative() 
					|| nonNegativeNonZeroLinearCombinationExists(sInvariants));
		}
	}
	
	public static void checkIfStructurallyBounded(PTNetStructuralProperties properties) {
		if (properties.isConservative() != null && properties.isConservative()) {
			properties.setStructurallyBounded(true);
		} else if (properties.getIncidenceMatrix().length > 0) {
			TssSolver tssSolver = new TssSolver(properties.getIncidenceMatrix(), -1);
			int[][] tss = tssSolver.solve();
			properties.setStructurallyBounded(positiveLinearCombinationExists(tss));
		}
	}
	
	public static void findTInvariants(PTNetStructuralProperties properties) {
		if (properties.getTransposedIncidenceMatrix().length > 0) {
			TssSolver tssSolver = new TssSolver(properties.getTransposedIncidenceMatrix(), 0);
			int[][] tss = tssSolver.solve();
			properties.setTInvariants(tss);
		} else {
			properties.setTInvariants(new int[0][]);
		}
	}
	
	public static void checkIfConsistent(PTNetStructuralProperties properties) {
		if (properties.getTransposedIncidenceMatrix().length > 0) {
			int[][] tInvariants = properties.getTInvariants();
			properties.setConsistent(positiveLinearCombinationExists(tInvariants));
			properties.setPartiallyConsistent(properties.isConsistent() 
					|| nonNegativeNonZeroLinearCombinationExists(tInvariants));
		}
	}
	
	public static void checkIfRepetitive(PTNetStructuralProperties properties) {
		if (properties.isConsistent() != null && properties.isConsistent()) {
			properties.setRepetitive(true);
			properties.setPartiallyRepetitive(true);
		} else if (properties.getTransposedIncidenceMatrix().length > 0) {
			TssSolver tssSolver = new TssSolver(properties.getTransposedIncidenceMatrix(), +1);
			int[][] tss = tssSolver.solve();
			properties.setRepetitive(positiveLinearCombinationExists(tss));
			properties.setPartiallyRepetitive(properties.isRepetitive() 
					|| nonNegativeNonZeroLinearCombinationExists(tss));
		}
	}
	
	private static boolean positiveLinearCombinationExists(int[][] tss) {
		if (tss.length == 0) {
			return false;
		}
		int size = tss[0].length;
		boolean isPositive[] = new boolean[size];
		int numPositive = 0;
		for (int i = 0; i < tss.length; i++) {
			for (int j = 0; j < size; j++) {
				if (!isPositive[j] && tss[i][j] > 0) {
					isPositive[j] = true;
					numPositive++;
					if (numPositive == size) {
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private static boolean nonNegativeNonZeroLinearCombinationExists(int[][] tss) {
		if (tss.length == 0) {
			return false;
		}
		int size = tss[0].length;
		boolean isNonNegative[] = new boolean[size];
		int numNonNegative = 0;
		boolean foundPositive = false;
		for (int i = 0; i < tss.length; i++) {
			for (int j = 0; j < size; j++) {
				if (!isNonNegative[j] && tss[i][j] >= 0) {
					isNonNegative[j] = true;
					numNonNegative++;
					if (!foundPositive && tss[i][j] > 0) {
						foundPositive = true;
					}
					if (numNonNegative == size && foundPositive) {
						return true;
					}
				}
			}
		}
		return false;
	}

}
