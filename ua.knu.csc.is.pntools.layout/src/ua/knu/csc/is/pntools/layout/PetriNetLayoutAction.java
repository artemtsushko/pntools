package ua.knu.csc.is.pntools.layout;

import ua.knu.csc.is.pntools.common.AbstractPetriNetAction;

public class PetriNetLayoutAction extends AbstractPetriNetAction {

	@Override
	protected PetriNetLayoutJob createJob() {
		return new PetriNetLayoutJob(this.petrinet, this.defaultInput);
	}

}
