package ua.knu.csc.is.pntools.layout;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.ElementTreeSelectionDialog;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.model.BaseWorkbenchContentProvider;
import org.eclipse.ui.model.WorkbenchLabelProvider;
import org.graphstream.algorithm.Toolkit;
import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.ui.geom.Point3;
import org.graphstream.ui.layout.Layout;
import org.graphstream.ui.layout.springbox.implementations.SpringBox;
import org.pnml.tools.epnk.helpers.NetFunctions;
import org.pnml.tools.epnk.pnmlcoremodel.Arc;
import org.pnml.tools.epnk.pnmlcoremodel.Coordinate;
import org.pnml.tools.epnk.pnmlcoremodel.Graphics;
import org.pnml.tools.epnk.pnmlcoremodel.Node;
import org.pnml.tools.epnk.pnmlcoremodel.NodeGraphics;
import org.pnml.tools.epnk.pnmlcoremodel.Object;
import org.pnml.tools.epnk.pnmlcoremodel.Page;
import org.pnml.tools.epnk.pnmlcoremodel.PetriNet;
import org.pnml.tools.epnk.pnmlcoremodel.PetriNetDoc;
import org.pnml.tools.epnk.pnmlcoremodel.PnmlcoremodelFactory;
import org.pnml.tools.epnk.pntypes.extension.PetriNetTypeExtensions;

import ua.knu.csc.is.pntools.common.AbstractPetriNetJob;


public class PetriNetLayoutJob extends AbstractPetriNetJob {
	private static final String PLUGIN_ID = "ua.knu.csc.is.pntools.layout";

	private Map<Page, Map<Node, String>> nodesByPage = new HashMap<>();
	private PetriNet petrinet;
	private String outputDirectory;
	private String outputFile;
	private IStatus errorStatus;

	public PetriNetLayoutJob(PetriNet petrinet, String outputDirectory) {
		super("pntools: Layout");
		this.petrinet = EcoreUtil.copy(petrinet);
		this.outputDirectory = outputDirectory;
		this.outputFile = this.petrinet.getId() + "_document.pnml";
	}
	
	@Override
	public boolean prepare() {
		if (!obtainDirectory() || !obtainFileName()) {
			return false;
		}
		for (Page page: petrinet.getPage()) {
			if (page.getId() == null) {
				MessageDialog.openError(null, getName(), 
						"Petri net pages must have ids");
				return false;
			}
			Map<Node, String> nodeIds = new HashMap<>();
			for (Object object : NetFunctions.getAllPageObjects(page)) {
				if (object instanceof Node) {
					Node node = (Node) object;
					String id = node.getId();
					if (id == null) {
						MessageDialog.openError(null, getName(),
								"Petri net nodes (places, transitions) must have ids");
						return false;
					}
					if (nodeIds.put(node, node.getId()) != null) {
						MessageDialog.openError(null, getName(),
								"Petri net node ids must be unique");
						return false;
					}
				}
			}
			nodesByPage.put(page, nodeIds);
		}
		return true;
	}
	
	private boolean obtainDirectory() {
		ElementTreeSelectionDialog dialog = new ElementTreeSelectionDialog(
				shell, new WorkbenchLabelProvider(), new BaseWorkbenchContentProvider());
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		dialog.setInput(root);
		if (outputDirectory != null) {
			IProject project = root.getProject(outputDirectory);
			if (project.isOpen()) {
				dialog.setInitialSelection(project);
			}
		}
		dialog.setAllowMultiple(false);
		dialog.setTitle(getName());
		dialog.setMessage("Select project");
		dialog.setValidator(
				selection ->  selection.length == 1 && selection[0] instanceof IProject
				? new Status(IStatus.OK, PLUGIN_ID,
						"Output PNML file will be created in the selected project.") 
				: new Status(IStatus.ERROR, PLUGIN_ID,
						"Please select a project.")
				);
		if (dialog.open() == Window.OK) {
			IProject project = (IProject) dialog.getFirstResult();
			this.outputDirectory = project.getName();
			return true;
		} else {
			return false;
		}
	}
	
	private boolean obtainFileName() {
		InputDialog dlg = new InputDialog(null, getName(), "Enter file name", outputFile, 
				input -> input.endsWith(".pnml") ? null : "File name should have .pnml extension");
		if (dlg.open() == Window.OK) {
			outputFile = dlg.getValue();
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		monitor.beginTask("Calculating Petri net layout", nodesByPage.size() + 2);
		try {
			nodesByPage.forEach((page, nodeIds) -> {
				monitor.subTask("Calculating Petri net layout for page " + page.getId());
				calculatePageLayout(page, nodeIds);
				monitor.worked(1);
			});
			
			if (monitor.isCanceled()) {
				return Status.CANCEL_STATUS;
			}
			
			monitor.subTask("Writing results to file");
			IFile file = writeResultsToFile();
			monitor.worked(1);
			
			monitor.subTask("Opening output file in editor");
			openFileInEditor(file);
			monitor.worked(1);
			monitor.done();
		} catch (Exception e) {
			e.printStackTrace();
			return new Status(IStatus.ERROR, PLUGIN_ID, "An error occurred", e);
		} 
		if (this.errorStatus != null) {
			// set from another thread
			return errorStatus;
		}
		showResults();
		return new Status(IStatus.OK, PLUGIN_ID, "Success!");
	}
	
	private void calculatePageLayout(Page page, Map<Node, String> nodeIds) {
		Graph graph = new MultiGraph(page.getId());
		nodeIds.values().forEach(graph::addNode);
        nodeIds.forEach((node, sourceId) -> {
        	for (Arc arc : node.getOut()) {
        		String targetId = arc.getTarget().getId();
        		graph.addEdge(sourceId + "_" + targetId, sourceId, targetId);
        	}
        });
        Layout layout = new SpringBox();
        Toolkit.computeLayout(graph, layout, 1);
        Point3 lowPoint = layout.getLowPoint();
        lowPoint.scale(-1);
        nodeIds.forEach((node, id) -> {
        	Point3 point = Toolkit.nodePointPosition(graph, id);
        	point.move(lowPoint);
        	setPosition(node, point);
        });
	}
	
	private void setPosition(Node node, Point3 point) {
		Graphics graphics = node.getGraphics();
		NodeGraphics nodeGraphics;
		if (graphics != null && graphics instanceof NodeGraphics) {
			nodeGraphics = (NodeGraphics) graphics;
		} else {
			nodeGraphics = PnmlcoremodelFactory.eINSTANCE.createNodeGraphics();
			node.setGraphics(nodeGraphics);
		}
		Coordinate position = nodeGraphics.getPosition();
		if (position == null) {
			position = PnmlcoremodelFactory.eINSTANCE.createCoordinate();
			nodeGraphics.setPosition(position);
		}
		position.setX((float) point.x * 200);
		position.setY((float) point.y * 200);
	}
	
	private IFile writeResultsToFile() throws IOException {
		IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(outputDirectory);
		IFile file = project.getFile(outputFile);
		final URI uri = URI.createURI(file.getFullPath().toString());
		ResourceSet resourceSet = new ResourceSetImpl();
		final Resource resource = resourceSet.createResource(uri);
		PetriNetDoc doc = PnmlcoremodelFactory.eINSTANCE.createPetriNetDoc();
		this.petrinet.setType(PetriNetTypeExtensions.getInstance()
				.createPetriNetType("http://www.pnml.org/version-2009/grammar/ptnet"));
		doc.getNet().add(this.petrinet);
		resource.getContents().add(doc);
		resource.save(new HashMap<String,Object>());
		return file;
	}
	
	private void openFileInEditor(IFile file) {
		shell.getDisplay().syncExec(() -> {
			IWorkbenchPage page =
				PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage();
			try {
				IDE.openEditor(page, file, true);
			} catch (PartInitException e) {
				setErrorStatus(new Status(IStatus.ERROR, PLUGIN_ID, 
						"Failed to open " + outputFile + " in editor", e));
			}
		});
	}
	
	private void setErrorStatus(IStatus status) {
		this.errorStatus = status;
	}
	
	protected void doShowResults() {
		MessageDialog.openInformation(null, getName(), 
				"Result Petri net has been written to file\n" + outputFile);
	}
	
	@Override
	public String getInput() {
		return this.outputDirectory;
	}

}
